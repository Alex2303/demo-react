import React, { Component } from 'react';
import Sidebar from './Sidebar';
import Head from 'next/head';

class Layout extends React.Component {
    render() {
        return (
            <div>
                <Head>
                    <title>React</title>
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui/dist/semantic.min.css" />
                    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
                </Head>
                <Sidebar />
            </div>
        )
    }
}

export default Layout





