import react, { useState } from 'react';

function Sidebar(){
    const [open, setOpen] = useState(false)

    return(
        <div>
            <nav className={open ? "open" : null}>
                <ul>
                    <li>test</li>
                    <li>test</li>
                    <li>test</li>
                </ul>
            </nav>
            <button className="hamburger-btn" onClick={() => { setOpen(!open) } }>Click</button>
            {/* au click sur le bouton, setOpen change la valeur initial false en true et active la classe open dans nav */}
        </div>
        
    )
}
    


export default Sidebar;