import React from 'react'

import '@fullcalendar/common/main.css'
import '@fullcalendar/daygrid/main.css'
import '@fullcalendar/timegrid/main.css'
import '../css/style.css'

//on a importé ici les fichiers css nécessaires pour l'affichage du calendrier
//ceux-ci ne pouvant pas être importés directement depuis les fichiers js dans node_modules

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}


