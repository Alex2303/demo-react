import Layout from '../components/Layout';
import Sidebar from '../components/Sidebar';
import Calendar from '../components/Calendar';

const Index = () => (
    <div>
        <Layout />
        <Sidebar />
        <Calendar />
    </div>
    
);


export default Index;

